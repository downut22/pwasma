package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JPanel;

import cells.Cell;

public class GamePanel extends JPanel
{
	public ArrayList<Cell> cells;
	
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);
        
        Toolkit.getDefaultToolkit().sync();
    }
	
	void doDrawing(Graphics g)
	{
		for(int i = 0; i < cells.size();i++)
		{
			cells.get(i).draw(g);
		}
	}
	
	public GamePanel(ArrayList<Cell> arr)
	{
		super();
		cells = arr;
		setBackground(new Color(50,50,50,255));
	}
}
