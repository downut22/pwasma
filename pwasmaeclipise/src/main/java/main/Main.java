package main;

import javax.swing.*;

import cells.Cell;

import java.awt.*;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;



public class Main {

	static ArrayList<Cell> cells;
	static int cellCount = 20;
	
	public static void main(String[] args) 
	{	
		cells = new ArrayList<Cell>();
		
		JFrame window = new JFrame();
		
	    window.setBounds(0, 0, 1000, 1000);
	    window.getContentPane().setBackground( new Color(50,50,50,1) );
	    
		for(int i = 0 ;i < cellCount;i++)
		{
			Cell c = new Cell(window);
			
			cells.add(c);
		}
	    
		GamePanel panel = new GamePanel(cells);
		window.add(panel);
		
	    window.setVisible(true);
	    
	    while(true)
	    {
	    	for(int i = 0 ;i < cellCount;i++)
			{
	    		cells.get(i).update(window,cells);		
			}
	    	
	    	try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    	
	    	SwingUtilities.updateComponentTreeUI(window);
	    }
	    
	}

}
