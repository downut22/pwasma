package cells;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;
import java.awt.*;

public class Cell
{
	double posX; double posY;
	double sizeX; double sizeY;
	public double speedX; public double speedY;
	public double orientation;
	double eyeSize = 0.2;
	
	Color color;
	public Color eyeColor;
	
	public boolean isStuck;
	CellBehaviour behaviour;
	
	double eyeX() {
		return (Math.cos(orientation*Math.PI*2)*sizeX*0.5) ;
	}
	double eyeY()
	{
		return (Math.sin(orientation*Math.PI*2)*sizeY*0.5) ;
	}
	
	public double directionX() {  return Math.cos(orientation*Math.PI*2);}
	public double directionY() {  return Math.sin(orientation*Math.PI*2);}
	
	public double eyePosX() {return posX + eyeX();}
	public double eyePosY() {return posY + eyeY();}
	
	void move(JFrame frame)
	{
		posX += speedX;
		
		if(posX + (sizeX*0.5) >= frame.getBounds().width) {
			posX = frame.getBounds().width - (sizeX*0.5) ;
			isStuck = true;
		}
		else if(posX - (sizeX*0.5) <= 0) {
			posX = sizeX*0.5;
			isStuck = true;
		}
		
		posY += speedY;
		if(posY + (sizeY*0.5) >= frame.getBounds().height) {
			posY = frame.getBounds().height - (sizeY*0.5) ;
			isStuck = true;
		}
		else if(posY - (sizeY*0.5) <= 0) {
			posY = sizeY*0.5;
			isStuck = true;
		}
	}
	
	public void update(JFrame frame,ArrayList<Cell> cells)
	{
		behaviour.Apply(cells);
		move(frame);
	}
	
	public void draw(Graphics g)
	{
		g.setColor(color);
		g.fillOval ((int)(posX-(sizeX*0.5)), (int)(posY-(sizeY*0.5)),(int)(sizeX),(int)(sizeY));  
		g.setColor(Color.black);
		g.drawOval ((int)(posX-(sizeX*0.5)), (int)(posY-(sizeY*0.5)),(int)(sizeX),(int)(sizeY)); 
		g.setColor(eyeColor);
		g.fillOval((int)(posX+(eyeX()*0.85)-(eyeSize*sizeX*0.5)),(int)(posY+(eyeY()*0.85)-(eyeSize*sizeY*0.5)),(int)(sizeX*eyeSize),(int)(sizeX*eyeSize));
	}
	
	
	public Cell(JFrame frame)
	{		
		Random random = new Random();
	
		this.posX = random.nextDouble() * frame.getBounds().width; this.posY = random.nextDouble() * frame.getBounds().height;
		
		this.sizeX = minSize + ((maxSize-minSize) * random.nextDouble()); 
		this.sizeY = this.sizeX;//minSize + ((maxSize-minSize) * random.nextDouble());
		
		this.orientation = random.nextDouble();
		color = new Color((int)(random.nextDouble()*255),(int)(random.nextDouble()*255),(int)(random.nextDouble()*255),255);
		
		behaviour = new CellBehaviour(this);
		
		System.out.println("Creating Cell at position :  X = " + posX + " Y = " + posY);		
	}
	
	static double minSize = 40; static double maxSize = 60;
	
}
