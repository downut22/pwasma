package cells;

import java.util.ArrayList;
import java.util.Random;
import java.awt.*;

public class CellBehaviour {

	public double moveSpeed;
	public double rotationSpeed = 0.01;
	public double rotationPreference = 0.4;
	public double visionDistance ;
	
	Cell cell;
	
	void Flee()
	{
		
	}
	void Attack(Cell victim)
	{
		double vX = victim.posX - cell.posX;
		double vY = victim.posY - cell.posY;
		double vN = Math.sqrt((vX*vX) + (vY*vY));
		vX /= vN; vY/=vN;
		
		double cX = cell.directionX();
		double cY = cell.directionY();
		
		cell.orientation = Math.acos( 
				((victim.posX*cell.posX) + (victim.posY*cell.posY)) / 
				( Math.sqrt((victim.posX*victim.posX) + (victim.posY*victim.posY))* Math.sqrt((victim.posX*cell.posX) + (cell.posY*cell.posY)))
				);
	}
	void Idle()
	{
		if(cell.isStuck) 
		{
			cell.orientation += rotationSpeed;
		}

		cell.orientation += rotationSpeed * (random.nextDouble()-rotationPreference);
		
		cell.speedX = Math.cos(cell.orientation*Math.PI*2) * moveSpeed;
		cell.speedY = Math.sin(cell.orientation*Math.PI*2) * moveSpeed;
	}
	
	public void Apply(ArrayList<Cell> cells)
	{
		int d = Detection(cells);
		if(d >= 0) 
		{
			Attack(cells.get(d));
			cell.eyeColor = Color.red;
		}
		else 
		{
			Idle();
			cell.eyeColor = Color.white;
		}
	}
	
	
	int Detection(ArrayList<Cell> cells)
	{
		for(int i = 0 ; i< cells.size();i++)
		{
			if(Math.abs(cells.get(i).posX - cell.eyePosX()) + Math.abs(cells.get(i).posY - cell.eyePosY()) <= visionDistance)
			{
				return i ;
			}
		}
		
		return -1;
	}
	
	public CellBehaviour(Cell cell)
	{
		this.cell = cell;
		
		random = new Random();
		
		rotationSpeed = minRotationSpeed + (maxRotationSpeed-minRotationSpeed)*random.nextDouble();
		rotationPreference = minRotationPref + (maxRotationPref-minRotationPref)*random.nextDouble();
		
		visionDistance = 10; //minVisionDistance + (maxVisionDistance-minVisionDistance)*random.nextDouble();
		
		moveSpeed = minMoveSpeed + (maxMoveSpeed-minMoveSpeed)*random.nextDouble();
	}

	Random random;
	static double minMoveSpeed = 0.5;static double maxMoveSpeed = 3;
	static double minRotationSpeed = 0.001; static double maxRotationSpeed = 0.02; 
	static double minRotationPref = 0.1; static double maxRotationPref = 0.9; 
	static double minVisionDistance = 20; static double maxVisionDistance = 300;
	
	static double stuckDuration = 100;
}
